from django.shortcuts import render_to_response
from django.template.context import RequestContext


def landing_page(request):
    context = {}
    return render_to_response('quizapp/index.html', RequestContext(request, context))
