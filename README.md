INSTALL
-------

sudo apt-get install -y git-core python-dev python-pip
sudo apt-get install -y postgresql pgadmin3 postgresql-contrib libpq-dev
sudo apt-get install -y nginx gunicorn

